# Fibonacci Rest API

Fibonacci Rest API is an application which returns the value of the given index

## Installation

Clone project

```bash
git clone
```
Install dependencies
```bash
npm install
```
[INSTALL POSTMAN](https://www.postman.com/)


## Usage
### In postman create a new request of type post and in the url enter http://localhost:3000/api/v1/fibonacci and in the body send a json with the following information.

### {"index": "Desired index number"} for example {"index": 5}

## Construido con 🛠️

* [ExpressJS](https://expressjs.com/es/) - The web framework used
* [NPM](https://www.npmjs.com/) - Dependency handler