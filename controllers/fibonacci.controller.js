exports.fibonacci_sequence = (req, res) => {
	const index = String(req.body.index);

	let fibonacci = [];
	fibonacci[0] = 0;
	fibonacci[1] = 1;
	for (var i = 2; i < 1000; i++) {
		fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
	}

	const index_value = fibonacci[index];
	res.status(200).send({"index_value": index_value});
};