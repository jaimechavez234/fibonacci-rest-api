const url = "/api/v1";

module.exports = app => {
    const fibonacci = require("../controllers/fibonacci.controller.js");

    app.post(`${url}/fibonacci`, fibonacci.fibonacci_sequence);
};